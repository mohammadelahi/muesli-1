#
#                                 M U E S L I   v 1.8
#
#
#     Copyright 2020 IMDEA Materials Institute, Getafe, Madrid, Spain
#     Contact: muesli.materials@imdea.org
#     Author: Ignacio Romero (ignacio.romero@imdea.org)
#
#     This file is part of MUESLI.
#
#     MUESLI is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     MUESLI is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with MUESLI.  If not, see <http://www.gnu.org/licenses/>.
#

cmake_minimum_required(VERSION 2.8)

project(muesli)

include_directories ( "." )

file(GLOB SRC_FILES
    "*.cpp"
    "Damage/*.cpp"
    "Failure/*.cpp"
    "Fcoupled/*.cpp"
    "Finitestrain/*.cpp"
    "Fluid/*.cpp"
    "Interface/*.cpp"
    "Math/*.cpp"
    "Scoupled/*.cpp"
    "Smallstrain/*.cpp"
    "Thermal/*.cpp"
    "Utils/*.cpp"
)

file(GLOB HEADER_FILES
    "*.h"
    "Damage/*.h"
    "Failure/*.h"
    "Fcoupled/*.h"
    "Finitestrain/*.h"
    "Fluid/*.h"
    "Interface/*.h"
    "Math/*.h"
    "Scoupled/*.h"
    "Smallstrain/*.h"
    "Thermal/*.h"
    "Utils/*.h"
)

add_library( muesli ${SRC_FILES} ${HEADER_FILES} )

# Only install Release Version
if(NOT WIN32)
    if (CMAKE_BUILD_TYPE STREQUAL "Release")
        install(TARGETS muesli
            LIBRARY DESTINATION lib
            ARCHIVE DESTINATION lib)
        install(FILES ${HEADER_FILES} DESTINATION include/muesli)
    else()
      message("Do not install not Release versions")
    endif()
else()
    message("Install on Windows not supported yet. (TODO)")
endif()

add_executable( test_muesli Test/test.cpp )
target_link_libraries ( test_muesli muesli )
